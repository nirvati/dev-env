$DEV_ROOT = Split-Path -Parent -Path $MyInvocation.MyCommand.Definition
Set-Location -Path $DEV_ROOT

$DEV_ROOT_PATH = $DEV_ROOT.replace("\", "/")
minikube start --driver=hyperv --iso-url="file://$DEV_ROOT_PATH/minikube-amd64.iso"

Write-Host "Installing Longhorn..."
kubectl apply -f "https://raw.githubusercontent.com/longhorn/longhorn/v1.6.0/deploy/longhorn.yaml"

Write-Host "Installing helm chart controller"
kubectl apply -f "https://github.com/k3s-io/helm-controller/releases/download/v0.15.8/deploy-cluster-scoped.yaml"

Write-Host "Installing Traefik..."
helm repo add traefik https://traefik.github.io/charts
helm repo update
helm install traefik traefik/traefik --create-namespace --namespace traefik

while (-not (kubectl get crd settings.longhorn.io)) {
  Start-Sleep -Seconds 5
}

Write-Host "Installing Nirvati init..."
kubectl apply -f init-dev.yml

# Wait for the chartmuseum service to be created
while (-not (kubectl get svc -n admin-chartmuseum main -o jsonpath='{.spec.clusterIP}')) {
  Start-Sleep -Seconds 5
}

@"
CHARTMUSEUM_URL=http://$((kubectl get svc -n admin-chartmuseum main -o jsonpath='{.spec.clusterIP}')):8080
APPS_ENDPOINT=http://localhost:3001
CORE_ENDPOINT=http://localhost:3002
HTTPS_ENDPOINT=http://localhost:3003
DATABASE_URL=postgresql://nirvati:development@$((kubectl get svc -n admin-nirvati postgres -o jsonpath='{.spec.clusterIP}')):5432/nirvati
INSTANCE_TYPE=lite
JWT_SECRET=development
APPS_DIR=$DEV_ROOT/apps
APPS_PATH=$DEV_ROOT/apps
LONGHORN_ENDPOINT=http://$((kubectl get svc -n longhorn-system longhorn-backend -o jsonpath='{.spec.clusterIP}')):9500
NIRVATI_SEED=development
TAILSCALE_SOCKET=/var/run/tailscale/tailscaled.sock
"@ | Out-File -FilePath ".env" -Encoding utf8

Copy-Item -Path ".env" -Destination "app-manager/.env"
Copy-Item -Path ".env" -Destination "core/.env"
Copy-Item -Path ".env" -Destination "https/.env"
Copy-Item -Path ".env" -Destination "api/.env"

Add-Content -Path "app-manager/.env" -Value "BIND_ADDRESS=127.0.0.1:3001"
Add-Content -Path "core/.env" -Value "BIND_ADDRESS=127.0.0.1:3002"
Add-Content -Path "https/.env" -Value "BIND_ADDRESS=127.0.0.1:3003"
Add-Content -Path "api/.env" -Value "BIND_ADDRESS=127.0.0.1:3004"
Add-Content -Path "dashboard/.env" -Value "GRAPHQL_ENDPOINT_CLIENT=http://127.0.0.1:3004/v0/graphql"
Add-Content -Path "dashboard/.env" -Value "GRAPHQL_ENDPOINT_SERVER=http://127.0.0.1:3004/v0/graphql"

Write-Host "====================================="
Write-Host "============== NIRVATI =============="
Write-Host "====== DEVELOPMENT ENVIRONMENT ======"
Write-Host "====================================="
Write-Host
Write-Host "Your development environment is now set up and ready to use"
