# Nirvati development environment

This is a simple local test environment for Nirvati, which uses [minikube](https://minikube.sigs.k8s.io/docs/start/) to create a local Kubernetes cluster.

It is still very much in development, so the following things are missing:

- Proper tailscale integration (Right now, you need to have tailscale installed and running on your machine)
- Proper access to the ingress for apps

## Pre-requisites

- [minikube](https://minikube.sigs.k8s.io/docs/start/)
- [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
- Docker Engine (Not Desktop)
- [Rust](https://rustup.rs/)
- [cargo-watch](https://crates.io/crates/cargo-watch)

## Development

First, you need to set up the virtual cluster with `./init.sh`.

Then, you need to run this to set up networking:

```bash
sudo ip route add 10.244.0.0/24 via $(minikube ip)
sudo ip route add 10.96.0.0/12 via $(minikube ip)
```

To start the environment, run these commands in separate terminal windows/tabs:

```bash
# In the api dir
cargo watch -x run --features=dotenvy,__development
# In the app-manager dir
cargo watch -x run --features=dotenvy
# In the core dir
cargo watch -x run --features=dotenvy
# In the https dir
cargo watch -x run --features=dotenvy
# In the dashboard dir
bun i # This will not keep running, run bun dev and bun dev:gql after this is finished
bun dev
# Also in the dashboard dir
bun dev:gql
```

Then, go to localhost:3000 in your browser and you should see the dashboard.

To finish the setup when you're required to visit your HTTPS domain, just visit localhost:3000/finish after the dashboard has started instead.

Also, `source setenv` is probably helpful if you want to use the `k` command as alias for kubectl.

## Stopping the development environment

To delete the routes, run this:

```bash
sudo ip route delete 10.244.0.0/24 via $(minikube ip)
sudo ip route delete 10.96.0.0/12 via $(minikube ip)
```

Then stop the cluster with


```bash
minikube delete
```
