#!/usr/bin/env bash

set -e

DEV_ROOT=$(dirname $(realpath $BASH_SOURCE))
cd $DEV_ROOT

minikube start --driver=kvm2 --iso-url=file://$DEV_ROOT/minikube-amd64.iso

if ! command -v krew &> /dev/null; then
echo "Installing Krew"
(
  set -x; cd "$(mktemp -d)" &&
  OS="$(uname | tr '[:upper:]' '[:lower:]')" &&
  ARCH="$(uname -m | sed -e 's/x86_64/amd64/' -e 's/\(arm\)\(64\)\?.*/\1\2/' -e 's/aarch64$/arm64/')" &&
  KREW="krew-${OS}_${ARCH}" &&
  curl -fsSLO "https://github.com/kubernetes-sigs/krew/releases/latest/download/${KREW}.tar.gz" &&
  tar zxvf "${KREW}.tar.gz" &&
  ./"${KREW}" install krew
);
fi

echo "Installing Longhorn..."
kubectl apply -f https://raw.githubusercontent.com/longhorn/longhorn/v1.6.0/deploy/longhorn.yaml

echo "Installing helm chart controller"
kubectl apply -f https://github.com/k3s-io/helm-controller/releases/download/v0.15.8/deploy-cluster-scoped.yaml

echo "Installing Traefik..."
helm repo add traefik https://traefik.github.io/charts
helm repo update
helm install traefik traefik/traefik --create-namespace --namespace traefik

while [ -z "$(kubectl get crd settings.longhorn.io)" ]; do
  sleep 5
done

echo "Installing Nirvati init..."
kubectl apply -f init-dev.yml

# Wait for the chartmuseum service to be created
while [ -z "$(kubectl get svc -n admin-chartmuseum main -o jsonpath='{.spec.clusterIP}')" ]; do
  sleep 5
done

cat <<EOF > .env
CHARTMUSEUM_URL=http://$(kubectl get svc -n admin-chartmuseum main -o jsonpath='{.spec.clusterIP}'):8080
APPS_ENDPOINT=http://localhost:3001
CORE_ENDPOINT=http://localhost:3002
HTTPS_ENDPOINT=http://localhost:3003
DATABASE_URL=postgresql://nirvati:development@$(kubectl get svc -n admin-nirvati postgres -o jsonpath='{.spec.clusterIP}'):5432/nirvati
INSTANCE_TYPE=lite
JWT_SECRET=development
APPS_DIR=${DEV_ROOT}/apps
APPS_PATH=${DEV_ROOT}/apps
LONGHORN_ENDPOINT=http://$(kubectl get svc -n longhorn-system longhorn-backend -o jsonpath='{.spec.clusterIP}'):9500
NIRVATI_SEED=development
TAILSCALE_SOCKET=/var/run/tailscale/tailscaled.sock
EOF

cp .env app-manager/.env
cp .env core/.env
cp .env https/.env
cp .env api/.env

echo "BIND_ADDRESS=127.0.0.1:3001" >> app-manager/.env
echo "BIND_ADDRESS=127.0.0.1:3002" >> core/.env
echo "BIND_ADDRESS=127.0.0.1:3003" >> https/.env
echo "BIND_ADDRESS=127.0.0.1:3004" >> api/.env
echo "GRAPHQL_ENDPOINT_CLIENT=http://127.0.0.1:3004/v0/graphql" >> dashboard/.env
echo "GRAPHQL_ENDPOINT_SERVER=http://127.0.0.1:3004/v0/graphql" >> dashboard/.env

echo "====================================="
echo "============== NIRVATI =============="
echo "====== DEVELOPMENT ENVIRONMENT ======"
echo "====================================="
echo
echo "Your development environment is now set up and ready to use"