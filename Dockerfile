FROM ubuntu:23.10

WORKDIR /workspace

COPY kubefire/build/images/ubuntu/install.sh /workspace/bin/
COPY kubefire/scripts/install-prerequisites-*.sh /workspace/bin/

RUN ./bin/install.sh
RUN apt update && apt install -y cryptsetup open-iscsi
